<?php

namespace Eternity\Xray\Middleware;

use Closure;
use Eternity\Xray\Xray;

/**
 * Class RequestTracing
 * @package Eternity\Xray\Middleware
 */
class RequestTracing
{
    /**
     * @var \Eternity\Xray\Xray
     */
    private $xray;

    /**
     * @param \Eternity\Xray\Xray $xray
     */
    public function __construct(Xray $xray)
    {
        $this->xray = $xray;
    }

    /**
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Terminates a request/response cycle.
     */
    public function terminate($request, $response)
    {
        $this->xray->submitHttpTracer($response);
    }
}
