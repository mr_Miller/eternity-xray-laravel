<?php

namespace Eternity\Xray\Collectors;

/**
 * Trait Backtracer
 * @package Eternity\Xray\Collectors
 */
trait Backtracer
{
    /**
     * @return array
     */
    public function getBacktrace(): array
    {
        $stack = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS | DEBUG_BACKTRACE_PROVIDE_OBJECT, 50);
        $sources = [];
        foreach ($stack as $index => $trace) {
            $trace = $this->parseTrace($index, $trace);
            if ($trace !== '') {
                $sources[] = $trace;
            }
        }

        return array_slice(array_filter($sources), 0, 5);
    }

    /**
     * @param array $backtrace
     * @return string
     */
    public function getCallerClass(array $backtrace): string
    {
        $arr = explode('\\', $backtrace[0]);

        return end($arr);
    }

    /**
     * @param $index
     * @param array $trace
     * @return string
     */
    protected function parseTrace($index, array $trace): string
    {
        if (isset($trace['class']) && !$this->isExcludedClass($trace['class'])) {
            return $trace['class'] . ':' . ($trace['line'] ?? '?');
        }

        return '';
    }

    /**
     * @param $className
     * @return bool
     */
    protected function isExcludedClass($className): bool
    {
        $excludedPaths = [
            'Illuminate/Database',
            'Illuminate/Events',
            'Eternity/Xray',
        ];

        $normalizedPath = str_replace('\\', '/', $className);
        foreach ($excludedPaths as $excludedPath) {
            if (strpos($normalizedPath, $excludedPath) !== false) {
                return true;
            }
        }

        return false;
    }
}
