<?php

namespace Eternity\Xray\Collectors;

/**
 * Class ViewCollector
 * @package Eternity\Xray\Collectors
 */
class ViewCollector extends EventsCollector
{
    /**
     * Register
     */
    public function registerEventListeners(): void
    {
        $this->app['events']->listen('creating:*', function ($view) {
            $viewName = substr($view, 10);
            $this->addSegment('View ' . $viewName)->end();
        });

        $this->app['events']->listen('composing:*', function ($view) {
            $viewName = substr($view, 11);
            if ($this->hasAddedSegment('View ' . $viewName)) {
                $this->endSegment('View ' . $viewName);
            }
        });
    }
}
