<?php

namespace Eternity\Xray\Collectors;

use Eternity\Xray\Segments\TimeSegment;
use Eternity\Xray\Segments\Trace;
use Illuminate\Support\Facades\Auth;
use Pkerrigan\Xray\Segment;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SegmentCollector
 * @package Eternity\Xray\Collectors
 */
class SegmentCollector
{
    use Backtracer;

    /**
     * @var array
     */
    protected $segments;

    /**
     * @return \Eternity\Xray\Segments\Trace
     */
    public function tracer(): Trace
    {
        return Trace::getInstance();
    }

    /**
     * @return \Pkerrigan\Xray\Segment
     */
    public function current(): Segment
    {
        return $this->tracer()->getCurrentSegment();
    }

    /**
     * @return bool
     */
    public function isTracerEnabled(): bool
    {
        return (bool)config('xray.enabled');
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function initHttpTracer(Request $request): void
    {
        if (!$this->isTracerEnabled()) {
            return;
        }

        $this->segments = [];
        $tracer = $this->tracer()
            ->setTraceHeader($_SERVER['HTTP_X_AMZN_TRACE_ID'] ?? null)
            ->setName(config('app.name') . ' HTTP')
            ->setClientIpAddress($request->getClientIp())
            ->addAnnotation('Framework', 'Laravel ' . app()->version())
            ->addAnnotation('PHP Version', PHP_VERSION)
            ->setUrl($request->url())
            ->setMethod($request->method());

        $tracer->begin();
    }

    /**
     * @param string $name
     */
    public function initCliTracer(string $name): void
    {
        if (!$this->isTracerEnabled()) {
            return;
        }

        $this->segments = [];
        $tracer = $this->tracer()
            ->setName(config('app.name') . ' CLI')
            ->addAnnotation('framework', 'Laravel ' . app()->version())
            ->setUrl($name);

        $tracer->begin();
    }

    /**
     * @param string $name
     * @param float|null $startTime
     * @param array|null $metadata
     * @return \Pkerrigan\Xray\Segment
     */
    public function addSegment(string $name, ?float $startTime = null, ?array $metadata = null): Segment
    {
        $segment = (new TimeSegment())->setName($name);

        if (null !== $metadata) {
            $segment->addMetadata('info', $metadata);
        }

        $this->current()->addSubsegment($segment);
        $segment->begin($startTime);
        $this->segments[$name] = $segment;

        return $segment;
    }

    /**
     * @param \Pkerrigan\Xray\Segment $segment
     * @param string $name
     * @return \Pkerrigan\Xray\Segment
     */
    public function addCustomSegment(Segment $segment, string $name): Segment
    {
        $this->current()->addSubsegment($segment);
        $segment->begin();
        $this->segments[$name] = $segment;

        return $segment;
    }

    /**
     * @param string $name
     * @return \Pkerrigan\Xray\Segment|null
     */
    public function getSegment(string $name): ?Segment
    {
        if ($this->hasAddedSegment($name)) {
            return $this->segments[$name];
        }

        return null;
    }

    /**
     * @param string $name
     */
    public function endSegment(string $name): void
    {
        if ($this->hasAddedSegment($name)) {
            $this->segments[$name]->end();

            unset($this->segments[$name]);
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasAddedSegment(string $name): bool
    {
        return \array_key_exists($name, $this->segments);
    }

    /**
     * Ends current segment
     */
    public function endCurrentSegment(): void
    {
        $this->current()->end();
    }

    /**
     * @param $response
     */
    public function submitHttpTracer($response): void
    {
        $submitterClass = config('xray.submitter');
        $tracer = $this->tracer();

        if (app()->bound(Auth::class) && Auth::check()) {
            $tracer->setUser((string)Auth::user()->getAuthIdentifier());
        }
        $tracer->end()
            ->setResponseCode($response->getStatusCode())
            ->submit(new $submitterClass());
    }

    /**
     * Submit CLI tracer
     */
    public function submitCliTracer(): void
    {
        $submitterClass = config('xray.submitter');
        $tracer = $this->tracer();

        if (app()->bound(Auth::class) && Auth::check()) {
            $tracer->setUser((string)Auth::user()->getAuthIdentifier());
        }
        $tracer->end()->submit(new $submitterClass());

        $tracer::flush();
    }
}
