<?php

namespace Eternity\Xray\Collectors;

use Eternity\Xray\Segments\SqlSegment;
use Illuminate\Database\Connection;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Database\Query\Expression;

/**
 * Class DatabaseQueryCollector
 * @package Eternity\Xray\Collectors
 */
class DatabaseQueryCollector extends EventsCollector
{
    /**
     * @var bool
     */
    protected $bindingsEnabled = false;

    /**
     * Register
     */
    public function registerEventListeners(): void
    {
        $this->app->events->listen(QueryExecuted::class, function (QueryExecuted $query) {
            $sql = $query->sql instanceof Expression ? $query->sql->getValue() : $query->sql;
            $this->handleQueryReport($sql, $query->bindings, $query->time, $query->connection);
        });

        $this->bindingsEnabled = config('xray.db_bindings');
    }

    /**
     * @param string $sql
     * @param array $bindings
     * @param float $time
     * @param \Illuminate\Database\Connection $connection
     */
    protected function handleQueryReport(string $sql, array $bindings, float $time, Connection $connection): void
    {
        if ($this->bindingsEnabled) {
            $sql = $this->parseBindings($sql, $bindings, $connection);
        }

        $backtrace = $this->getBacktrace();
        $this->current()->addSubsegment((new SqlSegment())
            ->setName($connection->getName() . ' at ' . $this->getCallerClass($backtrace))
            ->setDatabaseType($connection->getDriverName())
            ->setQuery($sql)
            ->addMetadata('backtrace', $backtrace)
            ->begin()
            ->end($time / 1000));
    }

    /**
     * @param string $sql
     * @param array $bindings
     * @param \Illuminate\Database\Connection $connection
     * @return string
     */
    private function parseBindings(string $sql, array $bindings, Connection $connection): string
    {
        $sql = str_replace(['%', '?'], ['%%', '%s'], $sql);

        $handledBindings = array_map(function ($binding) {
            if (is_numeric($binding)) {
                return $binding;
            }

            $value = str_replace(['\\', "'"], ['\\\\', "\'"], $binding);

            return "'{$value}'";
        }, $connection->prepareBindings($bindings));

        return vsprintf($sql, $handledBindings);
    }
}
