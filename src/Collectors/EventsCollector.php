<?php

namespace Eternity\Xray\Collectors;

use Illuminate\Foundation\Application;

/**
 * Class EventsCollector
 * @package Eternity\Xray\Collectors
 */
abstract class EventsCollector extends SegmentCollector
{
    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * @param \Illuminate\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        $this->registerEventListeners();
    }
}
