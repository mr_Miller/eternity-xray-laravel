<?php

namespace Eternity\Xray\Collectors;

/**
 * Class FrameworkCollector
 * @package Eternity\Xray\Collectors
 */
class FrameworkCollector extends EventsCollector
{
    /**
     * Register
     */
    public function registerEventListeners(): void
    {
        if (!$this->app->runningInConsole()) {
            $this->initHttpTracer($this->app['request']);
        }
    }
}
