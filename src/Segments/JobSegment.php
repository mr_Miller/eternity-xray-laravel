<?php

namespace Eternity\Xray\Segments;

use Pkerrigan\Xray\Segment;

/**
 * Class JobSegment
 * @package Eternity\Xray\Segments
 */
class JobSegment extends Segment
{
    /**
     * @var array
     */
    protected $payload;

    /**
     * @var bool
     */
    private $result;

    /**
     * @param array $payload
     * @return $this
     */
    public function setPayload(array $payload): JobSegment
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * @param bool $result
     * @return $this
     */
    public function setResult(bool $result): JobSegment
    {
        $this->result = $result;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        $data = parent::jsonSerialize();

        $data['job'] = array_filter([
            'payload' => $this->payload,
            'result'  => $this->result ? 'success' : 'failed',
        ]);

        return $data;
    }
}
