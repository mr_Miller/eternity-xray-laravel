<?php

namespace Eternity\Xray\Segments;

use Pkerrigan\Xray\Trace as BaseTrace;

/**
 * Class Trace
 * @package Eternity\Xray\Segments
 */
class Trace extends BaseTrace
{
    /**
     * @var static
     */
    private static $instance;

    /**
     * @return \Eternity\Xray\Segments\Trace|static
     */
    public static function getInstance(): Trace
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * Clears instance
     */
    public static function flush()
    {
        self::$instance = new static();
    }
}
