<?php

namespace Eternity\Xray\Segments;

use Pkerrigan\Xray\SqlSegment as BaseSegment;

/**
 * Class SqlSegment
 * @package Eternity\Xray\Segments
 */
class SqlSegment extends BaseSegment
{
    /**
     * @param float|null $startTime
     * @return $this|\Eternity\Xray\Segments\SqlSegment
     */
    public function begin(?float $startTime = null): SqlSegment
    {
        $this->startTime = $startTime ?? microtime(true);

        return $this;
    }

    /**
     * @param float|null $timeSpend
     * @return $this|\Eternity\Xray\Segments\SqlSegment
     */
    public function end(?float $timeSpend = null): SqlSegment
    {
        if ($timeSpend === null) {
            $this->endTime = microtime(true);
        } else {
            $this->endTime = $this->startTime + $timeSpend;
        }

        return $this;
    }
}
