<?php

namespace Eternity\Xray\Segments;

use Pkerrigan\Xray\Segment;

/**
 * Class TimeSegment
 * @package Eternity\Xray\Segments
 */
class TimeSegment extends Segment
{
    /**
     * @param float|null $time
     * @return $this|\Eternity\Xray\Segments\TimeSegment
     */
    public function begin(?float $time = null): TimeSegment
    {
        $this->startTime = $time ?? microtime(true);

        return $this;
    }
}
