<?php

namespace Eternity\Xray\Submission;

use Aws\XRay\XRayClient;
use Pkerrigan\Xray\Segment;
use Pkerrigan\Xray\Submission\SegmentSubmitter;

/**
 * Class APISegmentSubmitter
 * @package Eternity\Xray\Submission
 */
class APISegmentSubmitter implements SegmentSubmitter
{
    /**
     * @var \Aws\XRay\XRayClient
     */
    private $client;

    /**
     * Construct of APISegmentSubmitter
     */
    public function __construct()
    {
        $config = config('xray.aws');
        $config['credentials']['expires'] = now()->addDay()->unix();
        $this->client = new XRayClient($config);
    }

    /**
     * @param \Pkerrigan\Xray\Segment $segment
     */
    public function submitSegment(Segment $segment)
    {
        $this->client->putTraceSegments([
            'TraceSegmentDocuments' => [
                json_encode($segment->jsonSerialize()),
            ],
        ]);
    }
}
