<?php

namespace Eternity\Xray\Submission;

use Pkerrigan\Xray\Segment;
use Pkerrigan\Xray\Submission\DaemonSegmentSubmitter as SubmissionDaemonSegmentSubmitter;
use Pkerrigan\Xray\Submission\SegmentSubmitter;

/**
 * Class DaemonSegmentSubmitter
 * @package Eternity\Xray\Submission
 */
class DaemonSegmentSubmitter implements SegmentSubmitter
{
    /**
     * @var SubmissionDaemonSegmentSubmitter
     */
    private $submitter;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * Construct of DaemonSegmentSubmitter
     */
    public function __construct()
    {
        $this->host = env('ETERNITY_XRAY_DAEMON_ADDRESS');
        $this->port = (int)env('ETERNITY_XRAY_DAEMON_PORT', 2000);
    }

    /**
     * Get or create the Daemon submitter.
     *
     * @return SubmissionDaemonSegmentSubmitter
     */
    protected function submitter(): SubmissionDaemonSegmentSubmitter
    {
        if (is_null($this->submitter)) {
            $this->submitter = new SubmissionDaemonSegmentSubmitter(
                $this->host,
                $this->port
            );
        }

        return $this->submitter;
    }

    /**
     * @param Segment $segment
     * @return void
     */
    public function submitSegment(Segment $segment)
    {
        $this->submitter()->submitSegment($segment);
    }
}
