<?php

namespace Eternity\Xray;

use Eternity\Xray\Collectors\SegmentCollector;
use Pkerrigan\Xray\Segment;
use Pkerrigan\Xray\Trace;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Xray
 * @package Eternity\Xray
 */
class Xray
{
    /**
     * @var \Eternity\Xray\Collectors\SegmentCollector
     */
    private $collector;

    /**
     * @param \Eternity\Xray\Collectors\SegmentCollector $collector
     */
    public function __construct(SegmentCollector $collector)
    {
        $this->collector = $collector;
    }

    /**
     * @return \Pkerrigan\Xray\Trace
     */
    public function tracer(): Trace
    {
        return $this->collector->tracer();
    }

    /**
     * @return \Pkerrigan\Xray\Segment
     */
    public function current(): Segment
    {
        return $this->collector->current();
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->collector->isTracerEnabled();
    }

    /**
     * @param string $name
     * @param float|null $startTime
     * @param array|null $metadata
     * @return \Pkerrigan\Xray\Segment
     */
    public function addSegment(string $name, ?float $startTime = null, ?array $metadata = null): Segment
    {
        return $this->collector->addSegment($name, $startTime, $metadata);
    }

    /**
     * @param \Pkerrigan\Xray\Segment $segment
     * @param string $name
     * @return \Pkerrigan\Xray\Segment
     */
    public function addCustomSegment(Segment $segment, string $name): Segment
    {
        return $this->collector->addCustomSegment($segment, $name);
    }

    /**
     * @param string $name
     * @return \Pkerrigan\Xray\Segment|null
     */
    public function getSegment(string $name): ?Segment
    {
        return $this->collector->getSegment($name);
    }

    /**
     * @param string $name
     */
    public function endSegment(string $name): void
    {
        $this->collector->endSegment($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasAddedSegment(string $name): bool
    {
        return $this->collector->hasAddedSegment($name);
    }

    /**
     * End current segment
     */
    public function endCurrentSegment(): void
    {
        $this->collector->endCurrentSegment();
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function initHttpTracer(Request $request): void
    {
        $this->collector->initHttpTracer($request);
    }

    /**
     * @param string $name
     */
    public function initCliTracer(string $name): void
    {
        $this->collector->initCliTracer($name);
    }

    /**
     * @param $response
     */
    public function submitHttpTracer($response): void
    {
        $this->collector->submitHttpTracer($response);
    }

    /**
     * Submit CLI tracer
     */
    public function submitCliTracer(): void
    {
        $this->collector->submitCliTracer();
    }
}
